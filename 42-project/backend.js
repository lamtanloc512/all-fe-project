"use strict";
$(document).ready(function () {
  "use strict";
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  const gBASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/orders";
  const gCOLUMN_ID = 0;
  const gCOLUMN_KICH_CO = 1;
  const gCOLUMN_PIZZA_TYPES = 2;
  const gCOLUMN_DRINKS = 3;
  const gCOLUMN_TOTAL = 4;
  const gCOLUMN_HOVATEN = 5;
  const gCOLUMN_PHONE = 6;
  const gCOLUMN_STATUS = 7;
  const gCOLUMN_ACTION = 8;

  const gORDERS_COL = [
    "orderId",
    "kichCo",
    "loaiPizza",
    "idLoaiNuocUong",
    "thanhTien",
    "hoTen",
    "soDienThoai",
    "trangThai",
    "action",
  ];

  var gOrderDb = {
    orders: [],
    order: {},
    filterOrders: function (paramFilterObj) {
      "use strict";
      var vOrderFilterResult = [];
      vOrderFilterResult = this.orders.filter(function (paramOrderArr) {
        if (
          paramFilterObj.trangThai == "none" &&
          paramFilterObj.loaiPizza != "none" &&
          paramOrderArr.loaiPizza != null
        ) {
          console.log(paramFilterObj.loaiPizza.toLowerCase());
          console.log(paramOrderArr.loaiPizza.toLowerCase());
          return (
            paramOrderArr.loaiPizza.toLowerCase() ==
            paramFilterObj.loaiPizza.toLowerCase()
          );
        } else if (
          paramFilterObj.trangThai != "none" &&
          paramFilterObj.loaiPizza == "none" &&
          paramOrderArr.trangThai != null
        ) {
          return paramOrderArr.trangThai.includes(paramFilterObj.trangThai);
        } else if (
          paramFilterObj.trangThai != "none" &&
          paramFilterObj.loaiPizza != "none" &&
          paramOrderArr.trangThai != null &&
          paramOrderArr.loaiPizza != null
        ) {
          return (
            paramOrderArr.loaiPizza.includes(paramFilterObj.loaiPizza) &&
            paramOrderArr.trangThai.includes(paramFilterObj.trangThai)
          );
        } else if (
          paramFilterObj.trangThai == "none" &&
          paramFilterObj.loaiPizza == "none"
        ) {
          return paramOrderArr;
        }
      });
      console.log(vOrderFilterResult);
      return vOrderFilterResult;
    },
    updateOrder: function (paramOrder, paramStatus) {
      "use strict";
      getAllDataOrder(paramOrder);
      // neu bam nut confirmed se update confirmed, neeu cancel thi update cancel
      switch (paramStatus) {
        case "confirmed":
          paramOrder.trangThai = "confirmed";
          break;
        case "cancel":
          paramOrder.trangThai = "cancel";
          break;
      }

      console.log(paramOrder.id);
      let vIsValid = validate(paramOrder);
      if (vIsValid && gOrderInfo.orderId === paramOrder.orderId) {
        // them vao order id de truyen xuong ham ajax
        paramOrder.id = gOrderInfo.id;
        // goi ham xu ly ajax
        senDataToUpdateOrderInfo(paramOrder);
      }
    },
  };
  // định nghĩa table  - chưa có data
  var gOrderTable = $("#order-table").DataTable({
    ordering: false,
    // Khai báo các cột của datatable
    columns: [
      { data: gORDERS_COL[gCOLUMN_ID] },
      { data: gORDERS_COL[gCOLUMN_KICH_CO] },
      { data: gORDERS_COL[gCOLUMN_PIZZA_TYPES] },
      { data: gORDERS_COL[gCOLUMN_DRINKS] },
      { data: gORDERS_COL[gCOLUMN_TOTAL] },
      { data: gORDERS_COL[gCOLUMN_HOVATEN] },
      { data: gORDERS_COL[gCOLUMN_PHONE] },
      { data: gORDERS_COL[gCOLUMN_STATUS] },
      { data: gORDERS_COL[gCOLUMN_ACTION] },
    ],
    // Ghi đè nội dung của cột action, chuyển thành button chi tiết
    columnDefs: [
      {
        targets: gCOLUMN_ACTION,
        defaultContent:
          "<button class='info-order btn btn-info btn-sm'>Chi tiết</button>",
      },
    ],
  });

  var gOrderInfo = {
    id: "",
    orderId: "",
  };

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  //$("#order-detail-modal").modal('hide');
  onPageLoading();

  // render lai table thi close modal
  $("#div-order-detail").on("hidden.bs.modal", function () {
    "use strict";
    loadDataToTable(gOrderDb.orders);
  });

  // gán click event handler cho button chi tiet
  $("#order-table").on("click", ".info-order", function () {
    "use strict";
    $("#div-order-detail").modal("show");
    onButtonChiTietClick(this); // this là button được ấn
  });

  $("#btn-filter-order").on("click", function () {
    "use strict";
    onBtnFilterOrderClick();
  });

  $("#btn-confirm").on("click", () => {
    "use strict";
    onBtnConfirmClick();
  });

  $("#btn-cancel").on("click", () => {
    "use strict";
    onBtnCancelClick();
  });
  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  //khai bao ham xy ly khi an nut confirm
  function onBtnConfirmClick() {
    "use strict";
    let vStatus = "confirmed";
    gOrderDb.updateOrder(gOrderDb.order, vStatus);
  }

  //khai bao ham xu ly khi an nut cancel
  function onBtnCancelClick() {
    "use strict";
    let vStatus = "cancel";
    gOrderDb.updateOrder(gOrderDb.order, vStatus);
  }

  // infoFunction sẽ là function các nút cùng gọi
  function onButtonChiTietClick(paramChiTietButton) {
    "use strict";
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramChiTietButton).parents("tr");
    //Lấy datatable row
    var vDatatableRow = gOrderTable.row(vRowSelected);
    //Lấy data của dòng
    var vUserData = vDatatableRow.data();
    gOrderInfo.id = vUserData.id;
    gOrderInfo.orderId = vUserData.orderId;

    console.log(gOrderInfo);
    loadDataToModal(gOrderInfo);
  }

  // hàm chạy khi trang được load
  function onPageLoading() {
    "use strict";
    // lấy data từ server
    $.ajax({
      url: gBASE_URL,
      type: "GET",
      dataType: "json",
      async: false,
      success: function (responseObject) {
        gOrderDb.orders = responseObject;
        loadDataToTable(gOrderDb.orders);
        console.log(gOrderDb.orders);
      },
      error: function (error) {
        console.assert(error.responseText);
      },
    });
  }

  // hàm xử lý sự kiện filter users
  function onBtnFilterOrderClick() {
    "use strict";
    // Khai báo đối tượng chứa dữ liệu lọc trên form
    var vOrderFilterDataObj = {
      trangThai: "",
      loaiPizza: "",
    };
    // B1: Thu thập dữ liệu
    getFilterData(vOrderFilterDataObj);
    // B2: Validate (ko cần)
    // B3: Thực hiện nghiệp vụ lọc
    var vOrderFilterResult = gOrderDb.filterOrders(vOrderFilterDataObj);
    // B4: Hiển thị dữ liệu lên table
    loadDataToTable(vOrderFilterResult);
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
  // load data to table
  function loadDataToTable(paramResponseObject) {
    "use strict";
    //Xóa toàn bộ dữ liệu đang có của bảng
    gOrderTable.clear();
    //Cập nhật data cho bảng
    gOrderTable.rows.add(paramResponseObject);
    //Cập nhật lại giao diện hiển thị bảng
    gOrderTable.draw();
  }

  // hàm thu thập dữ liệu lọc trên form
  function getFilterData(paramFilterObj) {
    "use strict";
    paramFilterObj.trangThai = $("#input-order-status").val();
    paramFilterObj.loaiPizza = $("#input-pizza-types").val();
  }

  // hàm load data lên modal
  function loadDataToModal(paramData) {
    "use strict";
    if (paramData.orderId != "") {
      var vOrderDetail = sendParamToGetOrderDetail(paramData.orderId);

      $("#ipn-orderid").val(vOrderDetail.orderId);
      $("#ipn-kichco").val(vOrderDetail.kichCo);
      $("#ipn-duongkinh").val(vOrderDetail.duongKinh);
      $("#ipn-suon").val(vOrderDetail.suon);
      $("#select-drink").val(vOrderDetail.idLoaiNuocUong);
      $("#ipn-soluongnuoc").val(vOrderDetail.soLuongNuoc);
      $("#ipn-voucherid").val(vOrderDetail.idVourcher);
      $("#ipn-loai-pizza").val(vOrderDetail.loaiPizza);
      $("#ipn-salad").val(vOrderDetail.salad);
      $("#ipn-thanhtien").val(vOrderDetail.thanhTien);
      $("#ipn-giamgia").val(vOrderDetail.giamGia);
      $("#ipn-hoten").val(vOrderDetail.hoTen);
      $("#ipn-email").val(vOrderDetail.email);
      $("#ipn-phone").val(vOrderDetail.soDienThoai);
      $("#ipn-address").val(vOrderDetail.diaChi);
      $("#ipn-message").val(vOrderDetail.loiNhan);
      $("#ipn-status").val(vOrderDetail.trangThai);
      $("#ipn-ngaytao").val(vOrderDetail.ngayTao);
      $("#ipn-ngaycapnhat").val(vOrderDetail.ngayCapNhat);
    }
  }

  // function call api order detail
  function sendParamToGetOrderDetail(paramOrderId) {
    "use strict";
    $.ajax({
      type: "GET",
      url: gBASE_URL + "/" + paramOrderId,
      async: false,
      dataType: "json",
      success: function (responseObject) {
        gOrderDb.order = responseObject;
      },
      error: function (error) {
        console.assert(error.responseText);
      },
    });
    return gOrderDb.order;
  }

  // function call api update order
  function senDataToUpdateOrderInfo(paramOrder) {
    "use strict";
    $.ajax({
      type: "PUT",
      url: gBASE_URL + "/" + paramOrder.id,
      data: JSON.stringify(paramOrder),
      async: false,
      contentType: "application/json;charset=UTF-8",
      dataType: "json",
      success: function (response) {
        // duyet mang tim phan tu co id trung voi id tra ve va update lai
        gOrderDb.orders.map((bI, index) => {
          if (bI.id === response.id) {
            gOrderDb.orders[index] = response;
            loadDataToModal(gOrderDb.orders[index]);
          }
        });
        alert("Dữ liệu đã được lưu thành công");
      },
      error: function (error) {
        console.assert(error.responseText);
      },
    });
  }

  //function validate input value modal
  function validate(paramOrder) {
    "use strict";
    if (paramOrder.soDienThoai == "") {
      alert("Số điện thoại không được để trống");
      return false;
    }
    if (paramOrder.diaChi == "") {
      alert("Địa chỉ không được để trống");
      return false;
    }
    if (paramOrder.email == "") {
      alert("Email không được để trống");
      return false;
    }
    if (paramOrder.loaiPizza == "") {
      alert("Loại pizza không được để trống");
      return false;
    }
    if (paramOrder.salad == "") {
      alert("Salad không được để trống");
      return false;
    }
    if (paramOrder.idLoaiNuocUong == "") {
      alert("Loại nước không được để trống");
      return false;
    }
    if (paramOrder.suon == "") {
      alert("Sườn không được để trống");
      return false;
    }
    if (paramOrder.duongKinh == "") {
      alert("Đường kính không được để trống");
      return false;
    }
    if (paramOrder.kichCo == "") {
      alert("Kích cỡ không được để trống");
      return false;
    }
    if (paramOrder.hoTen == "") {
      alert("Họ và tên không được để trống");
      return false;
    }
    if (paramOrder.trangThai == "") {
      alert("Trạng thái không được để trống");
      return false;
    }
    return true;
  }

  function getAllDataOrder(paramOrderObj) {
    "use strict";
    paramOrderObj.orderId = $("#ipn-orderid").val().trim();
    paramOrderObj.kichCo = $("#ipn-kichco").val().trim();
    paramOrderObj.duongKinh = $("#ipn-duongkinh").val().trim();
    paramOrderObj.suon = $("#ipn-suon").val().trim();
    paramOrderObj.idLoaiNuocUong = $("#select-drink").val().trim();
    paramOrderObj.soLuongNuoc = $("#ipn-soluongnuoc").val().trim();
    paramOrderObj.idVourcher = $("#ipn-voucherid").val().trim();
    paramOrderObj.loaiPizza = $("#ipn-loai-pizza").val().trim();
    paramOrderObj.salad = $("#ipn-salad").val().trim();
    paramOrderObj.thanhTien = $("#ipn-thanhtien").val().trim();
    paramOrderObj.giamGia = $("#ipn-giamgia").val().trim();
    paramOrderObj.hoTen = $("#ipn-hoten").val().trim();
    paramOrderObj.email = $("#ipn-email").val().trim();
    paramOrderObj.soDienThoai = $("#ipn-phone").val().trim();
    paramOrderObj.diaChi = $("#ipn-address").val().trim();
    paramOrderObj.loiNhan = $("#ipn-message").val().trim();
    paramOrderObj.trangThai = $("#ipn-status").val().trim();
    paramOrderObj.ngayTao = $("#ipn-ngaytao").val().trim();
    paramOrderObj.ngayCapNhat = $("#ipn-ngaycapnhat").val().trim();
  }
});
