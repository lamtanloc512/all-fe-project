"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
let gFormData = {
  username: "",
  fname: "",
  lname: "",
};
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */

$("#btn-throw").on("click", () => {
  //B1: collect data
  let vFormData = getFormData();
  //B2: validate
  let vIsValid = validate(vFormData);
  if (vIsValid) {
    // B3: send request
    const vBASE_URL = "http://42.115.221.44:8080/devcamp-lucky-dice/";
    $.ajax({
      type: "POST",
      contentType: "application/json;charset=utf-8",
      url: vBASE_URL + "/dice",
      data: JSON.stringify(vFormData),
      dataType: "json",
      success: function (response) {
        // B4: neu co ket qua tra ve thi show frontend
        changeDiceImg(response);
      },
    });
  }
});

$("#btn-dice-history").on("click", () => {
  //B1: collect data
  let vFormData = getFormData();
  //B2: validate
  let vIsValid = validate(vFormData);
  if (vIsValid) {
    // B3: send request
    const vBASE_URL = "http://42.115.221.44:8080/devcamp-lucky-dice/";
    $.ajax({
      type: "GET",
      contentType: "application/json;charset=utf-8",
      url: vBASE_URL + "/dice-history?username=" + vFormData.username,
      success: function (response) {
        console.log(response);
        // B4: neu co ket qua tra ve thi show frontend
        showDiceHistoryToTable(response);
      },
    });
  }
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// khai bao ham getFormData
function getFormData() {
  try {
    gFormData.username = $("#inp-username").val();
    gFormData.fname = $("#inp-fname").val();
    gFormData.lname = $("#inp-lname").val();
  } catch (error) {
    console.log(error);
  }
  return gFormData;
}

// khai bao function validate
function validate(paramForm) {
  if (paramForm.username == "") {
    alert("Vui lòng nhập đầy đủ thông tin!");
    return false;
  }
  if (paramForm.fname == "") {
    alert("Vui lòng nhập đầy đủ thông tin!");
    return false;
  }
  if (paramForm.lname == "") {
    alert("Vui lòng nhập đầy đủ thông tin!");
    return false;
  }
  return true;
}

// khai bao ham chang img khi an nut nem
function changeDiceImg(paramRespone) {
  console.log(paramRespone);
  switch (paramRespone.dice) {
    case 1:
      $("#img-dice").prop("src", `./LuckyDiceImages/1.png`);
      break;
    case 2:
      $("#img-dice").prop("src", `./LuckyDiceImages/2.png`);
      break;
    case 3:
      $("#img-dice").prop("src", `./LuckyDiceImages/3.png`);
      break;
    case 4:
      $("#img-dice").prop("src", `./LuckyDiceImages/4.png`);
      break;
    case 5:
      $("#img-dice").prop("src", `./LuckyDiceImages/5.png`);
      break;
    case 6:
      $("#img-dice").prop("src", `./LuckyDiceImages/6.png`);
      break;
    default:
      $("#img-dice").prop("src", `./LuckyDiceImages/dice.png`);
      break;
  }
  switch (paramRespone.prize) {
    case "Mũ":
      $("#img-present").prop("src", "./LuckyDiceImages/mu.jpg");
      break;
    case "Xe máy":
      $("#img-present").prop("src", "./LuckyDiceImages/xe-may.jpg");
      break;
    case "Áo":
      $("#img-present").prop("src", "./LuckyDiceImages/ao.jpg");
      break;
    case "Ô tô":
      $("#img-present").prop("src", "./LuckyDiceImages/car.jpg");
      break;

    default:
      $("#img-present").prop("src", "");
      break;
  }

  if (paramRespone.voucher != null) {
    $("#p-voucherId").html(`${paramRespone.voucher.maVoucher}`);
    $("#p-percentDiscount").html(`${paramRespone.voucher.phanTramGiamGia}%`);
  }
  if (paramRespone.prize != null) {
    $("#h4-grat").html("Chúc mừng");
  }
}

//khai bao function khi an nut dice history hien ra bang
function showDiceHistoryToTable(paramRespone) {
  $("table tbody").html("");
  paramRespone.dices.forEach((bI, index) => {
    $("table tbody").append(
      `<tr>
        <th scope="row">${index + 1}</th>
        <td>${bI}</td>
       </tr>`
    );
  });
}
