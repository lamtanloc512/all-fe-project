"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBaseUrl = "http://42.115.221.44:8080/devcamp-pizza365/orders";
const gDrinkUrl = "http://42.115.221.44:8080/devcamp-pizza365/drinks";
const gAPI_VOUCHER_ENDPOINT =
  "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/";
let gObjectRequest = {
  kichCo: "",
  duongKinh: "",
  suon: "",
  salad: "",
  loaiPizza: "",
  idVourcher: "",
  idLoaiNuocUong: "",
  soLuongNuoc: "",
  hoTen: "",
  thanhTien: "",
  email: "",
  soDienThoai: "",
  diaChi: "",
  loiNhan: "",
};

let gVoucherRespone = {
  phanTramGiamGia: "",
  soTienPhaiThanhToan: "",
};

let gInfoOrder = null;

// khai báo các biến local, chứa giá trị của form input
let gInputNameOrder = document.getElementById("inputName");
let gInputEmailOrder = document.getElementById("inputEmail");
let gInputPhoneOrder = document.getElementById("inputPhone");
let gInputAddressOrder = document.getElementById("inputAddress");
let gInputMsgOrder = document.getElementById("inputMessage");
let gInputCouponOrder = document.getElementById("inputCoupon");
let gFormOrderSubmit = document.getElementById("form-data-submit");
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
onPageLoading();
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// function onpageloading
function onPageLoading() {
  // loadd all drink to select field
  onLoadDrinkToSelect();
  // onclickChangeColor
  onClickChangeColorMenuCombo();
  // onSelectDrinkChange value
  onSelectDrinkChange();
  // onClickChangeColorPizzaType
  onClickChangeColorPizzaType();
  //
  onSubmitOrderPizza();
}
//khai báo hàm ấn nút chuyển trang chi tiết đơn hàng
function onClickForwardToDetailOrder() {
  if (gInfoOrder.orderId == "") {
    alert("Bạn chưa tạo đơn hàng!");
  } else {
    let vNextPageUrl = "viewOrder.html";
    let vOpenUrlViewOrder =
      vNextPageUrl + "?orderid=" + gInfoOrder.orderId + "&id=" + gInfoOrder.id;
    window.location.href = vOpenUrlViewOrder;
  }
}
// Khai báo hàm xác nhận đơn hàng
function onClickConfirmOrder(paramDivInfo) {
  //truy xuất phần tử nút xác nhận
  let vBtnConfirm = document.querySelector("#btn-confirm");
  // thêm sự kiện click vào nút xác nhận
  vBtnConfirm.addEventListener("click", () => {
    // xóa thông tin đơn hàng sau khi đặt thành công, chuẩn bị cho lần đặt sau
    paramDivInfo.innerHTML = `Hãy cố gắng điền đủ các trường thông tin để đơn hàng có thể được
    ship nhanh nhất đến tay bạn nhé!`;
    //reset lại các trường nhập liệu cho các lần đặt hàng tiếp theo
    gInputNameOrder.value = "";
    gInputEmailOrder.value = "";
    gInputPhoneOrder.value = "";
    gInputAddressOrder.value = "";
    gInputMsgOrder.value = "";
    gInputCouponOrder.value = "";

    // reset lại màu các nút chọn menu
    let vBtnArrayMenuElements = document.querySelectorAll(
      ".btn.btn-primary.btn-choose-combo"
    );
    vBtnArrayMenuElements.forEach((vBtnElement) => {
      vBtnElement.className = "btn btn-success btn-choose-combo";
    });
    // reset lại màu các nút chọn loại pizza
    let vBtnArrayTypeElements = document.querySelectorAll(
      ".btn.btn-warning.btn-type-pizza.w-100"
    );
    vBtnArrayTypeElements.forEach((vBtnElement) => {
      vBtnElement.className = "btn btn-primary btn-type-pizza w-100";
    });
    sendRequestToCreateOrder(gObjectRequest);
  });
}

// onSelectDrinkChange()
function onSelectDrinkChange() {
  let vSelectDrink = document.getElementById("select-drink");
  vSelectDrink.addEventListener("change", (e) => {
    console.log(e.target.value);
    gObjectRequest.idLoaiNuocUong = e.target.value;
    console.log(gObjectRequest);
  });
}

// onLoadDrinkToSelect()
async function onLoadDrinkToSelect() {
  let vResquest = await fetch(gDrinkUrl, {
    method: "GET",
    headers: {
      "Content-Type": "application/json;charset=UTF-8",
    },
  });
  let vRespone = await vResquest.json();
  addOptionToSeclectField(vRespone);
}

function addOptionToSeclectField(paramDrinkRespone) {
  let vSelectDrink = document.getElementById("select-drink");
  // duyệt mảng các phần tử drink trả về
  paramDrinkRespone.forEach((bI) => {
    let vOption = document.createElement("option");
    vOption.setAttribute("value", `${bI.maNuocUong}`);
    vOption.innerText = bI.tenNuocUong;
    vSelectDrink.appendChild(vOption);
  });
}

//khai báo hàm thực thi khi ấn nút chuyển màu MENU COMBO(size pizza) + lay du lieu
function onClickChangeColorMenuCombo() {
  // lựa chọn tất cả phần tử có class là btn btn-success
  let vBtnArrayElements = document.querySelectorAll(
    ".btn.btn-success.btn-choose-combo"
  );
  vBtnArrayElements.forEach((vBtnElement) => {
    vBtnElement.addEventListener("click", (bIn) => {
      // chuyển màu cho nút
      bIn.target.className = "btn btn-primary btn-choose-combo";
      // sau khi đổi màu nút, ta sẽ có vài nút có class là btn-warning, để duyệt mảng các nút đó ta sử dụng []
      [].forEach.call(
        document.querySelectorAll(".btn-primary.btn-choose-combo"),
        (cIn) => {
          if (cIn == bIn.target) {
            getComboMenuPizza(cIn);
          }
          if (cIn !== bIn.target) {
            // khi ấn nút khác thì đổi màu phần tử button về như cũ
            cIn.className = "btn btn-success btn-choose-combo";
          }
        }
      );
    });
  });
}
// function onClickMenuCombo get data combo
function getComboMenuPizza(paramBtnChooseComboClicked) {
  switch (paramBtnChooseComboClicked.dataset.size) {
    case "small":
      (gObjectRequest.kichCo = "S"),
        (gObjectRequest.duongKinh = "20cm"),
        (gObjectRequest.suon = "2"),
        (gObjectRequest.salad = "200g"),
        (gObjectRequest.soLuongNuoc = "2");
      gObjectRequest.thanhTien = "150000";
      break;
    case "medium":
      (gObjectRequest.kichCo = "M"),
        (gObjectRequest.duongKinh = "25cm"),
        (gObjectRequest.suon = "4"),
        (gObjectRequest.salad = "300g"),
        (gObjectRequest.soLuongNuoc = "3");
      gObjectRequest.thanhTien = "200000";
      break;
    case "large":
      (gObjectRequest.kichCo = "L"),
        (gObjectRequest.duongKinh = "30cm"),
        (gObjectRequest.suon = "8"),
        (gObjectRequest.salad = "500g"),
        (gObjectRequest.soLuongNuoc = "4");
      gObjectRequest.thanhTien = "250000";
      break;
  }
  console.log(gObjectRequest);
}

// on click change color pizza type
// Khai báo hàm thực thi chuyển màu khi ấn nút chọn phần "CHỌN LOẠI PIZZA"
function onClickChangeColorPizzaType() {
  // lựa chọn tất cả phần tử có class là btn btn-info btn-type-pizza
  let vBtnArrayElements = document.querySelectorAll(
    ".btn.btn-primary.btn-type-pizza.w-100"
  );
  vBtnArrayElements.forEach((vBtnElement) => {
    vBtnElement.addEventListener("click", (bIn) => {
      // chuyển màu cho nút
      bIn.target.className = "btn btn-warning btn-type-pizza w-100";
      // sau khi đổi màu nút, ta sẽ có vài nút có class là btn-warning, để duyệt mảng các nút đó ta sử dụng []
      [].forEach.call(
        document.querySelectorAll(".btn.btn-warning.btn-type-pizza.w-100"),
        (cIn) => {
          if (cIn == bIn.target) {
            getPizzaType(cIn);
          }
          if (cIn !== bIn.target) {
            // khi ấn nút khác thì đổi màu phần tử button về như cũ
            cIn.className = "btn btn-primary btn-type-pizza w-100";
          }
        }
      );
    });
  });
}

// function onClickPizzaType get data pizzaType
function getPizzaType(paramBtnChoosePizzaTypeClicked) {
  gObjectRequest.loaiPizza = paramBtnChoosePizzaTypeClicked.dataset.pizza;
}

// Hàm thực thi khi ấn submit tạo đơn hàng
function onSubmitOrderPizza() {
  // Thuc thi ham này khi bấm gửi đơn
  gFormOrderSubmit.addEventListener("submit", (e) => {
    e.preventDefault();
    // khai bao mot object chua du lieu form
    gObjectRequest.hoTen = gInputNameOrder.value.trim();
    gObjectRequest.email = gInputEmailOrder.value.trim();
    gObjectRequest.soDienThoai = parseInt(gInputPhoneOrder.value.trim());
    gObjectRequest.diaChi = gInputAddressOrder.value.trim();
    gObjectRequest.loiNhan = gInputMsgOrder.value.trim();
    gObjectRequest.idVourcher = gInputCouponOrder.value.trim();

    // xử lý validate giá trị của form ở đây
    let isValidData = validDate(gObjectRequest);
    if (isValidData) {
      // Check mã giảm giá
      checkVoucherID(gObjectRequest);
    }
  });
}
//
function validDate(paramObjRequest) {
  // Check nếu khách hàng chưa chọn cỡ pizza
  if (paramObjRequest.kichCo == "") {
    alert("Vui lòng chọn kích cỡ pizza");
    return false;
  }
  if (paramObjRequest.idLoaiNuocUong == "") {
    alert("Vui lòng chọn đồ uống");
    return false;
  }
  if (paramObjRequest.loaiPizza == "") {
    alert("Vui lòng chọn loại pizza");
    return false;
  }
  if (paramObjRequest.hoTen == "") {
    alert("Vui lòng nhập họ và tên");
    return false;
  }
  if (paramObjRequest.email == "") {
    alert("Vui lòng nhập email");
    return false;
  }
  if (paramObjRequest.soDienThoai == "") {
    alert("Vui lòng nhập số điện thoại");
    return false;
  }
  if (paramObjRequest.diaChi == "") {
    alert("Vui lòng nhập địa chỉ giao hàng");
    return false;
  }
  return true;
}

// khai báo hàm check mã giảm giá
async function checkVoucherID(paramObjRequest) {
  if (paramObjRequest.idVourcher != "") {
    let vResquest = await fetch(
      gAPI_VOUCHER_ENDPOINT + paramObjRequest.idVourcher
    );
    if (vResquest.ok) {
      let vRespone = await vResquest.json();
      // cập nhật lại số tiền phải thanh toán nếu đúng mã giảm giá;
      gVoucherRespone.phanTramGiamGia = vRespone.phanTramGiamGia;
      gVoucherRespone.soTienPhaiThanhToan =
        paramObjRequest.thanhTien * ((100 - vRespone.phanTramGiamGia) / 100);
      // sau khi check xong mã giảm giá thì show data
      showDataOrder(gObjectRequest, gVoucherRespone);
    }
  } else {
    // cập nhật lại số tiền phải thanh toán nếu đúng mã giảm giá;
    gVoucherRespone.phanTramGiamGia = "0";
    gVoucherRespone.soTienPhaiThanhToan = paramObjRequest.thanhTien;
    // sau khi check xong mã giảm giá thì show data
    showDataOrder(gObjectRequest, gVoucherRespone);
  }
}

// Khai bao ham show data, có template để render ra dữ liệu trên frontend khi đã valid data
//show data
function showDataOrder(paramObjRequest, paramVoucher) {
  try {
    if (paramObjRequest !== null) {
      let vDivInfoOrder = document.getElementById("div-info-order");
      let vTemplateShow = `
          <div class="div-user-info">
            <p>Họ và tên: ${paramObjRequest.hoTen}</p>
            <p>Email: ${paramObjRequest.email}</p>
            <p>Điện thoại: ${paramObjRequest.soDienThoai}</p>
            <p>Địa chỉ: ${paramObjRequest.diaChi}</p>
            <p>Lời nhắn: ${paramObjRequest.loiNhan}</p>
          </div>
          <hr />
          <div class="div-pizza-size">
            <p>Kích cỡ: ${paramObjRequest.kichCo}</p>
            <p>Đường kính: ${paramObjRequest.duongKinh}</p>
            <p>Sườn nướng: ${paramObjRequest.suon}</p>
            <p>Salad: ${paramObjRequest.salad}</p>
            <p>Số lượng nước ngọt: ${paramObjRequest.soLuongNuoc}</p>
          </div>
          <hr />
          <div class="div-pizza-type">
            <p>Loại pizza: ${paramObjRequest.loaiPizza}</p>
            <p>Đồ uống: ${paramObjRequest.idLoaiNuocUong}</p>
            <p>Mã voucher: ${paramObjRequest.idVourcher}</p>
            <p>Giá VND: ${paramObjRequest.thanhTien}</p>
            <p>Discount: ${paramVoucher.phanTramGiamGia}% </p>
            <strong>Số tiền phải trả: ${paramVoucher.soTienPhaiThanhToan} VND</strong>
          </div>
          <a id="btn-confirm" class="btn btn-primary w-100 mt-5">Xác nhận đơn hàng</a>
        `;
      vDivInfoOrder.innerHTML = vTemplateShow;
      // chạy hàm xác nhận đơn hàng sau khi ấn nút xác nhận
      onClickConfirmOrder(vDivInfoOrder);
    }
  } catch (error) {
    console.log(error);
  }
}

// hàm xử lý call API tạo đơn hàng khi bấm xác nhận
async function sendRequestToCreateOrder(paramObjRequest) {
  let vDivInfoOrder = document.getElementById("div-info-order");
  let vRequest = await fetch(gBaseUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json;charset=UTF-8",
    },
    body: JSON.stringify(paramObjRequest),
  });

  if (vRequest.ok) {
    let vRespone = await vRequest.json();
    gInfoOrder = vRespone;
    // khi bấm nút xác nhận xong sẽ alert bạn đã đặt thành công
    alert("BẠN ĐÃ ĐẶT HÀNG THÀNH CÔNG");
    vDivInfoOrder.innerHTML = "Mã đơn hàng là: " + vRespone.orderId;
  }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
